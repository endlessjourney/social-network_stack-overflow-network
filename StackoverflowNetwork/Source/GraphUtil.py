from community import community_louvain
import Community_layout as comm
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import pandas as pd

def get_com_pos(Graph):
    best_partition = community_louvain.best_partition(Graph.to_undirected())
    pos = comm.community_layout(Graph.to_undirected(), best_partition)
    return best_partition, pos

def print_result(measures, measures_name):
    values = pd.DataFrame(measures.items(), columns=['node', 'value'])
    print(values)
    hight_value = {k: v for k, v in sorted(measures.items(), key=lambda item: item[1], reverse=True)}
    first_twenty_key = list(hight_value.items())[:20]
    print("Các đỉnh có giá trị {} lớn: {}".format(measures_name, first_twenty_key))
#     pd.DataFrame(first_twenty_key.items(), columns=['node', 'value'])
    
def draw(Graph, measures, measure_name, pos):
    nodes = nx.draw_networkx_nodes(Graph, pos=pos, node_size=250, cmap=plt.cm.plasma,
                                   node_color=list(measures.values()),
                                   nodelist=measures.keys())
    nodes.set_norm(mcolors.SymLogNorm(linthresh=0.01, linscale=1, base=10))
    edges = nx.draw_networkx_edges(Graph, pos=pos, width=0.1)

    plt.title(measure_name)
    plt.colorbar(nodes)
    plt.axis('off')
     
def draw_size(Graph, measures, measure_name, pos, scale, edge_width=0.2, DisplayLabel=False):
        node_size = [x * scale for x in measures.values()]
        node_size = [100 if x < 100 else x for x in node_size]
        nodes = nx.draw_networkx_nodes(Graph, pos=pos, node_size=node_size, cmap=plt.cm.plasma,
                                       node_color=list(measures.values()),
                                       nodelist=measures.keys())
        nodes.set_norm(mcolors.SymLogNorm(linthresh=0.01, linscale=1, base=10))
        if DisplayLabel:
            label = dict()
            for node in measures.keys():
                if measures[node] * scale > 450:
                    label[node] = node
            nx.draw_networkx_labels(Graph, pos, label, font_weight=100)
        nx.draw_networkx_edges(Graph, pos=pos, width=edge_width)
        plt.title(measure_name)
        plt.colorbar(nodes)
        plt.axis('off')