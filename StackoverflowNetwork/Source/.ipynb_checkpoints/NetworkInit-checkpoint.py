import networkx as nx
import json
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import Community_layout
import collections

class NetworkInit:

    def __init__(self, dir=None):
        if dir is None:
            pass
        else:
            self.file = open(dir, 'r')
            self.Graph = nx.DiGraph()
            self._AddNodes()

    def _AddNodes(self):
        Lines = self.file.readlines()
        for line in Lines:
            items = line.split()
            if len(items) == 0:
                continue
            if self.Graph.has_edge(items[0], items[1]):
                self.Graph[items[0]][items[1]]['weight'] += 1
            else:
                self.Graph.add_node(items[0])
                self.Graph.add_node(items[1])
                self.Graph.add_edge(items[0], items[1], weight=1)

    def SaveGraph(self, dir=None):
        data = nx.readwrite.node_link_data(self.Graph)
        if dir is None:
            with open('Graph.json', 'w+') as outfile:
                json.dump(data, outfile)
        else:
            with open(dir, 'w+') as outfile:
                json.dump(data, outfile)

    def ReadFromJson(self, JsonDir):
        with open(JsonDir, 'r') as GraphFile:
            GraphData = json.load(GraphFile)
            self.Graph = nx.readwrite.node_link_graph(GraphData)

    def Get_Number_Of_Nodes(self):
        print("Số lượng đỉnh trong mạng : ", len(self.Graph.nodes()))

    def Get_Number_Of_Edge(self):
        print("Số lượng cạnh trong mạng : ", len(self.Graph.edges()))

    def _get_com_pos(self):
        from community import community_louvain
        best_partition = community_louvain.best_partition(self.Graph.to_undirected())
        pos = GraphUtil.community_layout(self.Graph.to_undirected(), best_partition)
        return best_partition, pos

    def Community(self):
        best_partition, pos = self._get_com_pos()
        nx.draw(self.Graph, pos=pos, node_color=list(best_partition.values()), width=0.5)
        plt.show()

    def closeness_centrality(self):
        cs = nx.closeness_centrality(self.Graph)
        # In giá trị
        self._print_reslult(cs, "Closeness centrality")
        # Vẽ đồ thị
        _, pos = self._get_com_pos()
        self._draw(cs, "closeness centrality", pos)
        plt.show()

    def degree_centrality(self):
        # degree centrality
        dc = nx.degree_centrality(self.Graph)
        self._print_reslult(dc, "degree centrality")
        _, pos = self._get_com_pos()
        plot1 = plt.plot(1)
        self._draw_size(dc, "Degree centrality", pos, 7000, 0.1, True)
        # In degree centrality
        In_dc = nx.in_degree_centrality(self.Graph)
        # In giá trị
        self._print_reslult(In_dc, "In_degree centrality")
        # Vẽ đồ thị
        plot2 = plt.figure(2)
        self._draw_size(In_dc, "In-degree centrality", pos, 10000, 0.2, True)
        # Out degree centrality
        Out_dc = nx.out_degree_centrality(self.Graph)
        # In giá trị
        self._print_reslult(Out_dc, "Out_Degree centrality")
        # vẽ đồ thị
        plot3 = plt.figure(3)
        self._draw_size(Out_dc, "Out-degree centrality", pos, 10000, 0.2, True)
        plt.show()

    def _draw(self, measures, measure_name, pos):
        nodes = nx.draw_networkx_nodes(self.Graph, pos=pos, node_size=250, cmap=plt.cm.plasma,
                                       node_color=list(measures.values()),
                                       nodelist=measures.keys())
        nodes.set_norm(mcolors.SymLogNorm(linthresh=0.01, linscale=1, base=10))
        edges = nx.draw_networkx_edges(self.Graph, pos=pos, width=0.4)

        plt.title(measure_name)
        plt.colorbar(nodes)
        plt.axis('off')

    def _draw_size(self, measures, measure_name, pos, scale, edge_width=0.4, DisplayLabel=False):
        node_size = [x * scale for x in measures.values()]
        node_size = [100 if x < 100 else x for x in node_size]
        nodes = nx.draw_networkx_nodes(self.Graph, pos=pos, node_size=node_size, cmap=plt.cm.plasma,
                                       node_color=list(measures.values()),
                                       nodelist=measures.keys())
        nodes.set_norm(mcolors.SymLogNorm(linthresh=0.01, linscale=1, base=10))
        if DisplayLabel:
            label = dict()
            for node in measures.keys():
                if measures[node] * scale > 450:
                    label[node] = node
            nx.draw_networkx_labels(self.Graph, pos, label, font_weight=100)
        nx.draw_networkx_edges(self.Graph, pos=pos, width=edge_width)
        plt.title(measure_name)
        plt.colorbar(nodes)
        plt.axis('off')

    def degree_distribution(self):
        degree_sequence = sorted([d for n, d in self.Graph.degree()], reverse=True)
        degreeCount = collections.Counter(degree_sequence)
        plot1 = plt.figure(1)
        plt.plot(degreeCount.keys(), degreeCount.values(), 'ro', markersize=6)
        plt.ylabel("Count")
        plt.xlabel("Value")
        plt.title("degree Distribution")
        # In degree
        In_degree_sequence = sorted([d for n, d in self.Graph.in_degree()], reverse=True)
        In_degreeCount = collections.Counter(In_degree_sequence)
        plot2 = plt.figure(2)
        plt.plot(In_degreeCount.keys(), In_degreeCount.values(), 'ro', markersize=6)
        plt.ylabel("Count")
        plt.xlabel("Value")
        plt.title("In_degree Distribution")
        # out degree
        Out_degree_sequence = sorted([d for n, d in self.Graph.out_degree()], reverse=True)
        Out_degreeCount = collections.Counter(Out_degree_sequence)
        print(Out_degreeCount)
        plot3 = plt.figure(3)
        plt.plot(Out_degreeCount.keys(), Out_degreeCount.values(), 'ro', markersize=6)
        plt.ylabel("Count")
        plt.xlabel("Value")
        plt.title("Out_degree Distribution")
        # show
        plt.show()

    def PageRank(self):
        pageRank = nx.pagerank(self.Graph)
        # In giá trị
        self._print_reslult(pageRank, "PageRank")
        # Vẽ đồ thị
        _, pos = self._get_com_pos()
        plot1 = plt.figure(1)
        self._draw_size(pageRank, "PageRank Visualization", pos, 25000, 0.1, True)
        value = dict()
        for i in pageRank.values():
            if i in value:
                value[i] += 1
            else:
                value[i] = 1
        plot2 = plt.figure(2)
        plt.plot(value.keys(), value.values(), 'ro', markersize=6)
        plt.ylabel("Count")
        plt.xlabel("Value")
        plt.title("PageRank Distribution")
        plt.show()

    def Betweeness_Centrality(self):
        bc = nx.betweenness_centrality(self.Graph, weight='weight')
        # In giá trị
        self._print_reslult(bc, "Betweeness_Centrality")
        # Vẽ đồ thị
        _, pos = self._get_com_pos()
        self._draw_size(bc, "Betweeness centrality", pos, 20000, edge_width=0.1, DisplayLabel=True)
        plt.show()


    def _print_reslult(self, measures, measures_name):
        print("{} : {}".format(measures_name ,measures))
        hight_value = {k: v for k, v in sorted(measures.items(), key=lambda item: item[1], reverse=True)}
        first_twenty_key = list(hight_value.items())[:20]
        print("Các đỉnh có giá trị {} lớn: {}".format(measures_name ,first_twenty_key))
